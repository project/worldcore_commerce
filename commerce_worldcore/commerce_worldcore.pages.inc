<?php

/**
 * @file
 * User page callbacks for the WorldCore module.
 */

/**
 * Worldcore status_url processing.
 */
function commerce_worldcore_status_url() {

  $payment_method = commerce_payment_method_instance_load('commerce_worldcore|commerce_payment_commerce_worldcore');

  $headers = apache_request_headers();
  $json_body = file_get_contents('php://input');
  $hash_check = strtoupper(hash('sha256', $json_body . $payment_method['settings']['API_password']));
  // Hash checking.
  if ($headers['WSignature'] == $hash_check) {

    // Some code to confirm payment.
    $decoded_response = json_decode($json_body, TRUE);
    $transaction = commerce_payment_transaction_new('commerce_worldcore', $decoded_response['invoiceId']);
    $transaction->remote_id = $decoded_response['track'];
    $transaction->instance_id = $payment_method['instance_id'];
    $transaction->amount = commerce_currency_decimal_to_amount($decoded_response['amount'], $decoded_response['customField']);

    if ($decoded_response['status'] == 'Completed') {
      $transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
      $transaction->message = t('Payment successful.');
    }

    commerce_payment_transaction_save($transaction);

  }
  else {

    // Some code to log invalid payments.
    watchdog('WorldCore',
    'Hash mismatch! %WSignature vs. %hash_check',
    array(
      '%WSignature' => $headers['WSignature'],
      '%hash_check' => $hash_check,
    ),
    WATCHDOG_ERROR);

    die();

  }

}
